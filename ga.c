#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define POP 20
#define TAXA_XOVER 0.25
#define MUTACAO 0.01


int **populacao, **nova_populacao;
float *eval;
int x_bits, y_bits, n_bits;
float x_inf, x_sup, y_inf, y_sup;


void alocar_memoria(void);
void populacao_inicial(void);
int calcular_bits(int precisao, float min, float max);
float binario_para_decimal(int *cromossomo);
void avaliar_individuos(void);
float calcular_fitness(int *cromossomo);
int roleta(void);
void cruzamento(void);
void mutacao(void);
void nova_geracao(void);
void reportar(void);


void alocar_memoria(void) {
  int i;
  
  populacao = (int**) malloc(sizeof(int*) * POP);
  nova_populacao = (int**) malloc(sizeof(int*) * POP);
  eval = (float*) malloc(sizeof(float) * POP);
  
  for (i = 0; i < POP; i++) {
    populacao[i] = (int*) malloc(sizeof(int) * n_bits);
    nova_populacao[i] = (int*) malloc(sizeof(int) * n_bits);
  }
}

void populacao_inicial(void) {
  int i, j;
  
  for (i = 0; i < POP; i++) {
    for (j = 0; j < n_bits; j++) {    
      float random = (float) rand() / (float) RAND_MAX;
      int gene = random < 0.5 ? 1 : 0;
      populacao[i][j] = gene;
    }
  }
}

int calcular_bits(int precisao, float min, float max) {
  float tamanho;
  
  tamanho = max - min;
  tamanho = tamanho * pow(10, precisao);
  return (int) log2(tamanho) + 1;
}

float calcular_fitness(int *cromossomo) {
  int i;
  float x, x_decimal = 0;
  float y, y_decimal = 0;
  
  for (i = x_bits-1; i >= 0; i--)
    x_decimal = x_decimal + (cromossomo[i] * pow(2, (x_bits-1)-i));
  
  for (i = n_bits-1; i >= x_bits-1; i--)
    y_decimal = y_decimal + (cromossomo[i] * pow(2, (n_bits-1)-i));
  
  x = x_inf + x_decimal * ((x_sup - x_inf) / (pow(2,x_bits)-1));
  y = y_inf + y_decimal * ((y_sup - y_inf) / (pow(2,y_bits)-1));
  
  return 21.5 + x * sin(4*M_PI*x) + y * sin(20*M_PI*y);
}

void avaliar_individuos(void) {
  int i;
  
  for (i = 0; i < POP; i++)
    eval[i] = calcular_fitness(populacao[i]);
}

int roleta(void) {
  int i;
  float aux = 0, fitness_total = 0;
  
  for (i = 0; i < POP; i++)
    fitness_total += eval[i];
  
  float random = ((float) rand() / (float) RAND_MAX) * fitness_total;
  
  for (i = 0; (i < POP && aux < random); i++)
    aux += eval[i];
  
  i--;
  
  return i;
}

void cruzamento(void) {
  int organismo;
  int gene;
  int i1;
  int i2;
  int ponto_corte;
  
  for (organismo=0; organismo<POP; organismo+=2) {
    i1 = roleta();
    i2 = roleta();
    
    float random = ((float) rand() / (float) RAND_MAX);
    
    if (random < TAXA_XOVER) {
      ponto_corte = rand() % (n_bits-1) + 1;
      
      for (gene=0; gene<n_bits; gene++) {
        if (gene < ponto_corte) {
          nova_populacao[organismo][gene] = populacao[i1][gene];
          nova_populacao[organismo+1][gene] = populacao[i2][gene];
        } else {
          nova_populacao[organismo][gene] = populacao[i2][gene];
          nova_populacao[organismo+1][gene] = populacao[i1][gene];
        }
      }
    } else {
      memcpy(nova_populacao[organismo], populacao[i1], sizeof(int)*n_bits);
      memcpy(nova_populacao[organismo+1], populacao[i2], sizeof(int)*n_bits);
    }
  }
}

void mutacao(void) {
  int organismo;
  
  for (organismo = 0; organismo < POP; organismo++) {
    float random = ((float) rand() / (float) RAND_MAX);
    if (random < MUTACAO) {
      int gene = rand() % n_bits;
      int bit = (nova_populacao[organismo][gene] == 0) ? 1 : 0;
      nova_populacao[organismo][gene] = bit;
    }
  }
}

void nova_geracao(void) {
  int i, j;
  
  for (i = 0; i < POP; i++) {
    for (j = 0; j < n_bits; j++) {
      populacao[i][j] = nova_populacao[i][j];
    }
  }
}

void reportar(void) {
  int i, j;
  
  for (i = 0; i < POP; i++) {
    for (j = 0; j < n_bits; j++)
      printf("%d", populacao[i][j]);
    printf("\n");
  }
  
  for (i = 0; i < POP; i++)
    printf("eval[%d] => %f\n", i, eval[i]);
}

int main(int argc, char *argv[]) {
  x_inf = -3;
  x_sup = 12.1;
  y_inf = 4.1;
  y_sup = 5.8;
  
  srand( time(NULL) );
  
  alocar_memoria();
  
  x_bits = calcular_bits(4, x_inf, x_sup);
  y_bits = calcular_bits(4, y_inf, y_sup);
  n_bits = x_bits + y_bits;
  
  populacao_inicial();
  avaliar_individuos();
  
  int i;
  
  for (i = 0; i < 1000; i++) {
    cruzamento();
    nova_geracao();
    mutacao();
    avaliar_individuos();
  }
  
  reportar();
  
  return 0;
}

